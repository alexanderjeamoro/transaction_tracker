import format from "date-fns/format";

export default (_: any, inject: any) => {
  inject("currencyFormat", (amount: number, currency: string = "PHP", locale: string = "en-US") => {
    return new Intl.NumberFormat(locale, {
      style: 'currency', currency: currency
    }).format(amount);
  });

  inject("dateFormat", (date: Date, dateFormat: string = "MMM dd, Y") => {
    return format(date, dateFormat);
  });

  inject("numberFormat", (number: number, decimals: number = 2): string => {
    return Intl.NumberFormat('mathbold', {minimumFractionDigits: decimals})
      .format(number);
  })
}

