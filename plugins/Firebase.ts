import firebase from "firebase/app";
import 'firebase/auth'
import 'firebase/firestore'

const firebaseConfig = {
  apiKey: process.env.API_KEY,
  authDomain: process.env.AUTH_DOMAIN,
  projectId: process.env.PROJECT_ID,
  storageBucket: process.env.STORAGE_BUCKET,
  messagingSenderId: process.env.MESSAGING_SENDER_ID,
  appId: process.env.APP_ID,
  measurementId: process.env.MEASUREMENT_ID
};

!firebase.apps.length ? firebase.initializeApp(firebaseConfig) : '';

export const auth = firebase.auth();

const db = firebase.firestore();

const transactionsCollection = db.collection('transactions');
const walletsCollection = db.collection('wallets');
const settingCollection = db.collection('setting');
const walletTypesCollection = db.collection('wallet_types');
const currencyCollection = db.collection('currencies');
const categoryCollection = db.collection('categories');
const categoryTypeCollection = db.collection('categoryTypes');

export default firebase;
export {
  db,
  transactionsCollection,
  walletTypesCollection,
  walletsCollection,
  settingCollection,
  currencyCollection,
  categoryCollection,
  categoryTypeCollection
}


