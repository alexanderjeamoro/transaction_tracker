import Vue from "vue";
import {
  APPEND_TO_LIST,
  DELETE_ITEM_IN_LIST,
  PREPEND_TO_LIST,
  RESET_ALL,
  RESET_STATE_BY_KEY,
  SET_ACTIVE_ITEM,
  SET_ACTIVE_ITEM_VALUE_BY_KEY,
  SET_LIST,
  SET_ORIGINAL_ACTIVE_ITEM
} from "~/core/utils/mutation-types";
import { Activable, Listable, PrimaryID } from "~/core/contracts/Contractable"
import { MutationTree } from "vuex";

export const LIST = <T extends Listable<S>, S extends PrimaryID>(): object => ({
  [SET_LIST](state: T, payload: Listable<S>): void {
    Vue.set(state, "list", payload);
  },

  [APPEND_TO_LIST](state: T, payload: Array<S> | S): void {
    if (Array.isArray(payload)) {
      payload.forEach(item => {
        Vue.set(state.list, item.id, item);
      });
    } else
      Vue.set(state.list, payload.id, payload);
  },

  [PREPEND_TO_LIST](state: T, payload: S): void {
    if (Array.isArray(payload)) {
      payload.forEach(item => {
        Vue.set(state.list, item.id, item);
      });
    } else
      Vue.set(state.list, payload.id, payload);
  },

  [DELETE_ITEM_IN_LIST](state: Listable<S>, id: string): void {
    Vue.delete(state.list, id);
  }
})

export const RESET = <S extends { [x: string]: any }>(RESET_STATE: S): object => ({
  [RESET_ALL](state: S): void {
    Object.keys(RESET_STATE).forEach((key: string) => {
      Vue.set(state, key, RESET_STATE[key]);
    })
  },

  [RESET_STATE_BY_KEY](state: S, payload: Array<string> | string): void {
    if (Array.isArray(payload)) {
      payload.forEach((key: string) => {
        Vue.set(state, key, RESET_STATE[key]);
      })
    } else
      Vue.set(state, payload, RESET_STATE[payload]);
  }
});

export const ACTIVE_ITEM = <S extends Activable<any>, R extends { [x: string]: any }>(): object => ({
  [SET_ACTIVE_ITEM](state: S, payload: R): void {
    Vue.set(state, "active_item", payload);
  },

  [SET_ORIGINAL_ACTIVE_ITEM](state: S, payload: R): void {
    Vue.set(state, "original_active_item", payload);
  },

  [SET_ACTIVE_ITEM_VALUE_BY_KEY](state: S, payload: Array<Record<string, any>> | Record<string, any>): void {
    if (Array.isArray(payload)) {
      payload.forEach(item => {
        Vue.set(state.active_item, item.key, item.value);
      })
    } else
      Vue.set(state.active_item, payload.key, payload.value);
  }
});

