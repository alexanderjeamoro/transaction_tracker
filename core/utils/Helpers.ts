import Vue, { VNode } from "vue";

export function createSimpleFunctional(
  className: string,
  el = 'div',
  name?: string
) {
  return Vue.extend({
    name: name || className.replace(/__/g, '-'),

    functional: true,

    render(createElement, {data, children}): VNode {
      data.staticClass = (`${className} ${data.staticClass || ''}`).trim()

      return createElement(el, data, children)
    },
  })
}

export function isCssColor(color?: string | false): boolean {
  return !!color && !!color.match(/^(#|var\(--|(rgb|hsl)a?\()/)
}

export function convertToUnit(str: string | number | null | undefined, unit = 'px'): string | undefined {
  if (str == null || str === '') {
    return undefined
  } else if (isNaN(+str!)) {
    return String(str)
  } else {
    return `${Number(str)}${unit}`
  }
}

export function numberFormat(number: number, decimals: number = 2): string {
  const options = {
    currency: "PHP",
    minimumFractionDigits: decimals
  }
  return Intl.NumberFormat('mathbold', options)
    .format(number);
}
