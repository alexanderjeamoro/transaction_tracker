import { padStart } from "lodash";

const daysInWeek: number = 7;
const monthsInYear: number = 12;
const daysInMonths: Array<number> = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
const defaultLocale = {
  'en-US': {
    dow: 1,
    format: 'MM/DD/YYYY'
  },
}

export default class DatePicker {
  timezone: string;
  locale: string;
  monthData: Record<string, any>;
  weekdayNameShorter: Array<string>;
  weekdayNameShort: Array<string>;
  monthNamesLong: Array<string>;
  monthNamesShort: Array<string>;
  weekdayNameLong: Array<string>;
  daysInWeek: number;
  years: Array<number>;
  todayComps: Record<string, any>;

  constructor(timezone = "Asia/Manila") {
    this.locale = "en-GB";
    this.monthData = {}
    this.timezone = timezone;
    this.daysInWeek = daysInWeek;
    this.weekdayNameShorter = this.getDayNames("narrow");
    this.weekdayNameShort = this.getDayNames("short");
    this.weekdayNameLong = this.getMonthNames("long");
    this.monthNamesLong = this.getMonthNames("long");
    this.monthNamesShort = this.getMonthNames("short");
    this.years = this.getYears();
    this.todayComps = this.getCurrentMonthComponent();
  }

  getMonthComponents(month: number, year: number) {
    const key = `${month}-${year}`;
    let monthData = this.monthData[key];

    if (!monthData) {
      const isLeapYear = (year % 4 === 0 && year % 100 !== 0) || year % 400 === 0;
      const days = month === 2 && isLeapYear ? 29 : daysInMonths[month - 1];
      const firstDayOfMonth = new Date(year, month - 1, 1);
      const firstWeekday = firstDayOfMonth.getDay();

      monthData = {
        isLeapYear,
        days,
        firstWeekday,
        month,
        year,
      }

      this.monthData[key] = monthData;
    }

    return monthData
  }

  getDateFromParts(parts: Record<string, any>): Date {
    const date = new Date();
    const {
      year = date.getFullYear(),
      month = date.getMonth() + 1,
      day = date.getDate(),
      hours: hrs = 0,
      minutes: min = 0,
      seconds: sec = 0,
      milliseconds: ms = 0,
    } = parts;

    return new Date(year, month - 1, day, hrs, min, sec, ms);
  }

  getMonthDates(year: number = 2021): Array<Date> {
    let dates = [];

    for (let i = 0; i < monthsInYear; i++) {
      dates.push(new Date(year, i, 1));
    }

    return dates;
  }

  getWeekdayNames(): Array<Date> {
    let dates = [];

    const year = 2021;
    const month = 1;
    let date = 15;

    const d = new Date(year, month, date);
    date = date - d.getDay();

    for (let i = 0; i < daysInWeek; i++) {
      dates.push(new Date(year, month, date + i));
    }

    return dates;
  }

  getDayNames(length: "long" | "short" | "narrow") {
    const dateFormat = new Intl.DateTimeFormat(this.locale, {
      weekday: length
    });

    return this.getWeekdayNames().map(date => dateFormat.format(date))
  }

  getMonthNames(length: "numeric" | "2-digit" | "long" | "short" | "narrow") {
    const dateFormat = new Intl.DateTimeFormat(this.locale, {
      month: length
    });

    return this.getMonthDates().map(date => dateFormat.format(date));
  }

  getYears() {
    const now = new Date();
    const currentYear = now.getFullYear() + 30;
    const yearList = [];

    for (let year = currentYear; year >= currentYear - 130; year--) {
      yearList.push(year);
    }

    return yearList;
  }

  getDateParts(date: Date | null) {
    if (date === null)
      date = new Date();

    const month = date.getMonth() + 1;
    const year = date.getFullYear();
    const day = date.getDate();
    const weekday = date.getDay() + 1;

    return {
      date: this.getDateString(day, month, year),
      month,
      year,
      day,
      weekday
    }
  }

  getCurrentMonthComponent() {
    const {month, year, day, date} = this.getDateParts(new Date());
    const {days, firstWeekday} = this.getMonthComponents(month, year);

    return {
      date,
      month,
      year,
      day,
      days,
      firstWeekday
    };
  }

  getNextMonthComponents(month: number, year: number) {
    if (month === 12)
      return this.getMonthComponents(1, year + 1);

    return this.getMonthComponents(month + 1, year);
  }

  getPrevMonthComponents(month: number, year: number) {
    if (month === 1)
      return this.getMonthComponents(12, year - 1);

    return this.getMonthComponents(month - 1, year);
  }

  getDateString(day: number, month: number, year: number) {
    return `${year}-${padStart(String(month), 2, "0")}-${padStart(String(day), 2, "0")}`
  }

  generateCalendar({month, year}: Record<string, any>, selectedComponent: Record<string, any>) {
    const calendarStructure: Record<string, any> = {};
    const {firstWeekday, days} = this.getMonthComponents(month, year);

    for (let offset = 0; offset < firstWeekday; offset++) {
      calendarStructure[`d-offset-${offset}`] = {
        classes: [
          `v-date-picker__day`,
          `day-offset`
        ],
        key: `offset-${offset}`
      }
    }

    for (
      let day = 1, weekday = firstWeekday;
      day <= days;
      day++, weekday += weekday === daysInWeek ? 1 - daysInWeek : 1
    ) {
      const date = this.getDateString(day, month, year);

      calendarStructure[`day-${day}`] = {
        date,
        day,
        month,
        year,
        weekday,
        classes: [
          'v-date-picker__day'
        ],
        key: `day-${date}`
      }
    }

    if (month === selectedComponent.month && year === selectedComponent.year)
      calendarStructure [`day-${selectedComponent.day}`] ["classes"].push("is-selected");

    if (month === this.todayComps.month && year === this.todayComps.year)
      calendarStructure[`day-${this.todayComps.day}`]["classes"].push("is-today");

    return calendarStructure;
  }
}
