import * as TransactionContract from "~/core/contracts/Transactions";
import * as WalletContract from "~/core/contracts/WalletContract";
import * as Contractable from "~/core/contracts/Contractable";
import * as WalletType from "~/core/contracts/WalletTypeContract";
import { Currency, CurrencyState } from "~/core/contracts/Currency";
import { Category, CategoryState } from "~/core/contracts/CategoryContract";
import { CategoryTypeState } from "~/core/contracts/CategoryTypeContract";
import { Tag, TagState } from "~/core/contracts/Tag";

export const SEARCH_RESET = (): Contractable.Searchable => ({
  search: ""
})

export const LIST_RESET = <T>(): Contractable.Listable<T> => ({
  list: []
})

export const ACTIVE_ITEM = <T>(data: T): Contractable.Activable<T> => ({
  active_item: data,
  original_active_item: data
})

export const TRANSACTION_RESET = (): TransactionContract.Transaction => ({
  amount: null,
  categoryId: "",
  categoryName: "",
  id: "",
  remarks: "",
  tagId: "",
  tagName: "",
  transactionDate: null,
  userId: "",
  userNickname: "",
  walletId: "",
  walletName: "",
  currencyId: "",
  currencyCode: ""
});

export const TRANSACTION_RESET_STATE = (): TransactionContract.TransactionState => ({
  ...LIST_RESET<TransactionContract.Transaction>(),
  ...ACTIVE_ITEM<TransactionContract.Transaction>(TRANSACTION_RESET())
})

export const WALLET_RESET = (): WalletContract.Wallet => ({
  name: "",
  currencyId: "",
  currencyCode: "",
  currentBalance: 0,
  transactionsCount: 0,
  userId: "",
})

export const WALLET_STATE_RESET = (): WalletContract.WalletState => ({
  ...LIST_RESET<WalletContract.Wallet>()
})

export const WALLET_TYPE_STATE_RESET = (): WalletType.WalletTypeState => ({
  ...LIST_RESET<WalletType.WalletType>()
})

export const CURRENCY_STATE_RESET = (): CurrencyState => ({
  ...LIST_RESET<Currency>()
})

export const CATEGORY_RESET = (): Category => ({
  name: "",
  isUpdatable: true,
  transactionCount: 0,
  walletId: "",
  walletName: "",
  categoryTypeId: "",
  categoryTypeName: "",
  parentId: "",
  parentName: ""
})

export const CATEGORY_STATE_RESET = (): CategoryState => ({
  ...LIST_RESET<Category>()
})

export const CATEGORY_TYPE_STATE_RESET = (): CategoryTypeState => ({
  list: {}
})

export const TAG_RESET = (): Tag => ({
  id: "",
  name: ""
})

export const TAG_STATE_RESET = (): TagState => ({
  ...LIST_RESET<Tag>()
})
