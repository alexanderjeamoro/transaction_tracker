import { Selectable } from "~/core/contracts/Contractable";

export const Sort: Array<Selectable> = [
  {
    id: "asc",
    text: "Newest"
  },
  {
    id: "desc",
    text: "Oldest"
  }
]

export const Transactions: Array<Selectable> = [
  {
    id: "",
    text: "All Transactions"
  },
  {
    id: "debit",
    text: "Debit"
  },
  {
    id: "credit",
    text: "Credit"
  }
]

export const CategoryType: Array<Selectable> = [
  {
    id: "expense",
    text: "Expense"
  },
  {
    id: "income",
    text: "Income"
  },
  {
    id: "debt",
    text: "Debt"
  }
];


export const DayOfWeek: Array<any> = [
  {
    id: 0,
    letter: "S",
    abbr: "Sun",
    text: "Sunday"
  }, {
    id: 1,
    letter: "M",
    abbr: "Mon",
    text: "Monday"
  }, {
    id: 2,
    letter: "T",
    abbr: "Tue",
    text: "Tuesday"
  }, {
    id: 3,
    letter: "W",
    abbr: "Wed",
    text: "Wednesday"
  }, {
    id: 4,
    letter: "T",
    abbr: "Thu",
    text: "Thursday"
  }, {
    id: 5,
    letter: "F",
    abbr: "Fri",
    text: "Friday"
  }, {
    id: 6,
    letter: "S",
    abbr: "Sat",
    text: "Saturday"
  }
];

export const Months: Array<any> = [{
  id: 0,
  abbr: "Jan",
  text: "January"
}, {
  id: 1,
  abbr: "Feb",
  text: "February"
}, {
  id: 2,
  abbr: "Mar",
  text: "March"
}, {
  id: 3,
  abbr: "Apr",
  text: "April"
}, {
  id: 4,
  abbr: "May",
  text: "May"
}, {
  id: 5,
  abbr: "Jun",
  text: "June"
}, {
  id: 6,
  abbr: "Jul",
  text: "July"
}, {
  id: 7,
  abbr: "Aug",
  text: "August"
}, {
  id: 8,
  abbr: "Sep",
  text: "September"
}, {
  id: 9,
  abbr: "Oct",
  text: "October"
}, {
  id: 10,
  abbr: "Nov",
  text: "November"
}, {
  id: 11,
  abbr: "Dec",
  text: "December"
}];

export const Color: Array<string> = [
  "default",
  "primary",
  "success",
  "warning",
  "secondary",
  "danger",
  "dark"
];

export const Variants: Array<string> = [
  "outlined",
  "text",
  "plain"
];

export const Sizes: Array<string> = ["small", "x-small", "large", 'x-large'];

export const Align: Array<string> = ["start", "end", "center", "baseline", "stretch"];

export const Justify: Array<string> = ["start", "end", "center", "between", "around", "evenly"];

export const Direction: Array<string> = ["row", "row-reverse", "col", "col-reverse"];

export const ButtonTypes: Array<string> = ["button", "submit", "reset"];

export const Position: Array<string> = ["fixed", "relative", "absolute"];

export const PopperPlacement: Array<string> = ["top-start", "top-end", "bottom-start", "bottom-end", "right-start", "right-end", "left-start", "left-end"];
