import { maxLength, required } from "vuelidate/lib/validators";

const PostCategoryRequest = {
  name: {
    required,
    maxLength: maxLength(254)
  },

  categoryTypeId: {
    required
  },

  walletId: {
    required
  }
};


export {
  PostCategoryRequest
};
