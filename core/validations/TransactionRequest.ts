import { maxLength, required } from "vuelidate/lib/validators";

const PostTransactionRequest = {
  currencyId: {
    required
  },

  amount: {
    required
  },

  remarks: {
    required,
    maxLength: maxLength(254)
  },

  transactionDate: {
    required,
  },

  categoryId: {
    required
  },

  walletId: {
    required
  }
};


export {
  PostTransactionRequest
};
