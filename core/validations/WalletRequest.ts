import { maxLength, required } from "vuelidate/lib/validators";

const PostWalletRequest = {
  name: {
    required,
    maxLength: maxLength(191),
  },

  currencyId: {
    required
  },

  currentBalance: {
    required
  }
};

export {
  PostWalletRequest
};
