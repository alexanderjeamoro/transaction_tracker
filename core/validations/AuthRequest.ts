import { email, required } from "vuelidate/lib/validators";

export const AuthRequest = {
  email: {
    required,
    email
  },
  password: {
    required
  }
}
