import Vue, { VueConstructor } from 'vue';
import { Variants } from "~/core/utils/Variables";
import { camelCase, kebabCase } from "lodash";

export function factory(variants: Array<string>): VueConstructor {
  let props: Record<string, BooleanConstructor> = {};

  variants.forEach(variant => {
    props[camelCase(variant)] = Boolean;
  });

  return Vue.extend({
    props: props,

    computed: {
      variantClasses(): object {
        let classNode: Record<string, boolean> = {};

        variants.forEach(variant =>
          classNode[kebabCase(variant)] = this.$props[camelCase(variant)]
        );

        return classNode
      }
    }
  });
}

const Variantable = factory(Variants);

export default Variantable;
