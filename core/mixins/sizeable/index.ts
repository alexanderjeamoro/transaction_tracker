import Vue from 'vue'

const Sizeable = Vue.extend({
  name: 'sizeable',

  props: {
    small: Boolean,
    large: Boolean,
    xLarge: Boolean,
    xSmall: Boolean,
  },

  computed: {
    isDefault(): boolean {
      return Boolean(
        !this.small &&
        !this.large &&
        !this.xLarge &&
        !this.xSmall
      )
    },

    sizeableClasses(): Record<string, boolean> {
      return {
        'x-large': this.xLarge,
        'large': this.large,
        'default': this.isDefault,
        'small': this.small,
        'x-small': this.xSmall,
      }
    }
  }
});

export default Sizeable;
