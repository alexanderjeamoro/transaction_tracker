import Vue from "vue";
import { Color } from "~/core/utils/Variables";
import { isCssColor } from "~/core/utils/Helpers";

const Colorable = Vue.extend({
  name: 'colorable',

  props: {
    color: {
      type: String,
      validator(value: string) {
        return Color.includes(value) || isCssColor(value)
      },
    },
  },

  computed: {
    colorClasses(): Record<string, boolean> {
      return {
        [this.color]: Boolean(this.color)
      }
    }
  }
});

export default Colorable;
