import Vue from 'vue';

const Roundable = Vue.extend({
  name: 'roundable',

  props: {
    rounded: Boolean,
    tile: Boolean
  },

  computed: {
    roundedClasses(): Record<string, boolean> {
      const composite = [];

      if (this.tile) {
        composite.push('rounded-0');
      } else if (this.rounded) {
        composite.push('rounded');
      }

      return composite.length > 0 ? {
        [composite.join(' ')]: true
      } : {}
    }
  }
});

export default Roundable;
