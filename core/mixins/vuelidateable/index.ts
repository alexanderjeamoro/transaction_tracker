import _, { camelCase, capitalize, head } from "lodash";
import mixins from "~/core/utils/mixins";
import { validationMixin } from "vuelidate";

_.templateSettings.interpolate = /{{([\s\S]+?)}}/g;

export interface VuelidateMessageInterface {
  [x: string]: string;
}

const MESSAGES_MAP: VuelidateMessageInterface = {
  required: "The {{ attribute }} field is required.",
  maxLength: "The {{ attribute }} may not be greater than {{ max }}.",
  minLength: "The {{ attribute }} must be at least {{ min }}.",
  minValue: "The {{ attribute }} must be at least {{ min }}.",
  email: "The {{ attribute }} must be a valid email address."
};

const baseMixins = mixins(
  validationMixin
);

const Vuelidateable = baseMixins.extend({
  methods: {
    validationErrors($fieldName: string, $validation: any = null) {
      const key = camelCase($fieldName);

      if ($validation === null) {
        $validation = this.$v[key];
      }

      if (!$validation.$invalid || !$validation.$dirty) return [];

      const strReplace = {attribute: capitalize($fieldName)};

      return Object.keys($validation.$params).reduce(
        (errors: Array<string>, validator: string) => {
          if (!$validation[validator]) {
            const variables = _.omit($validation.$params[validator], ["type"]),
              compiled = _.template(MESSAGES_MAP[validator]);

            errors.push(compiled({...strReplace, ...variables}));
          }

          return errors;
        },
        []
      );
    },

    getFirstError(errors: any) {
      return head(errors);
    }
  }
});

export default Vuelidateable;
