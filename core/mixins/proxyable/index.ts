import Vue from "vue";

export function factory(prop = 'value', event = 'change') {
  return Vue.extend({
    name: "proxyable",

    model: {
      prop,
      event
    },

    props: {
      [prop]: {
        required: false
      }
    },

    data: () => ({
      sharedState: {
        [prop]: null
      }
    }),

    computed: {
      internalValue: {
        get(): any {
          return this.sharedState[prop];
        },
        set(value: any): void {
          if (value === this.sharedState[prop]) return;

          this.sharedState[prop] = value;
          this.$emit(event, value);
        }
      }
    },

    watch: {
      [prop]: {
        immediate: true,
        handler(value: any) {
          this.sharedState[prop] = value;
        }
      },
    },
  })
}

const Proxyable = factory();

export default Proxyable;
