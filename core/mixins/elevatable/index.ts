import Vue from 'vue';

const Elevatable = Vue.extend({
  name: 'elevatable',

  props: {
    elevation: Boolean
  },

  computed: {
    elevationClasses(): Record<string, boolean> {
      if (this.elevation) return {
        'shadow': true
      }

      return {};
    },
  },
})

export default Elevatable;
