import Vue, { VNodeData, PropType } from 'vue';

export default Vue.extend({
  name: 'routable',

  props: {
    activeClass: String,
    disabled: Boolean,
    link: Boolean,
    href: [String, Object],
    to: [String, Object],
    target: String,
    exact: {
      type: Boolean as PropType<boolean | undefined>,
      default: undefined
    }
  },

  computed: {
    isLink(): boolean {
      return this.to || this.href || this.link;
    }
  },

  methods: {
    generateRouteLink() {
      let tag;
      let data: VNodeData = {

      }

      if (this.to) {

      } else {
      }

      return { tag, data }
    }
  }
});
