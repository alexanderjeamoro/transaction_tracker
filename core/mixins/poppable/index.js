import mixins from "~/core/utils/mixins";
import Proxyable from "~/core/mixins/proxyable";
import { createPopper } from "@popperjs/core";
import ClickOutside from "~/core/directives/ClickOutside";
import { PopperPlacement } from "~/core/utils/Variables";

const baseMixins = mixins(
  Proxyable
);

const Poppable = baseMixins.extend({
  name: "poppable",

  directives: {
    ClickOutside
  },

  props: {
    placement: {
      type: String,
      default: "bottom-end",
      validator(value) {
        return PopperPlacement.includes(value);
      }
    },

    offset: {
      type: String,
      default: "6",
      validator(value) {
        return !isNaN(value);
      }
    }
  },

  data: () => ({
    popperInstance: {}
  }),

  computed: {
    poppableClasses() {
      return {
        'active': this.internalValue
      }
    }
  },

  mounted() {
    const activator = this.$refs.activator;
    const content = this.$refs.content;

    this.popperInstance = createPopper(activator, content, {
      placement: this.placement,
      modifiers: [
        {
          name: 'eventListeners',
          enabled: true
        }, {
          name: 'offset',
          options: {
            offset: [0, Number(this.offset)],
          }
        }],
    });
  },

  methods: {
    toggler() {
      this.internalValue ? this.close() : this.open();
    },

    open() {
      this.$nextTick(() => {
        this.internalValue = true;
        this.popperInstance.update();
      });
    },

    close() {
      this.$nextTick(() => {
        this.internalValue = false;
      })
    }
  }
})

export default Poppable;
