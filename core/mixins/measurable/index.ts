import Vue from "vue";
import { convertToUnit } from "~/core/utils/Helpers";

const Measurable = Vue.extend({
  name: "measurable",

  props: {
    maxHeight: [Number, String],
    height: [Number, String],
    minHeight: [Number, String],
    maxWidth: [Number, String],
    width: [Number, String],
    minWidth: [Number, String],
  },

  computed: {
    measurableStyles(): object {
      const styles: Record<string, string> = {}

      const maxHeight = convertToUnit(this.maxHeight);
      const height = convertToUnit(this.height);
      const minHeight = convertToUnit(this.minHeight);
      const maxWidth = convertToUnit(this.maxWidth);
      const width = convertToUnit(this.width);
      const minWidth = convertToUnit(this.minWidth);

      if (maxHeight) styles.maxHeight = maxHeight
      if (height) styles.height = height
      if (minHeight) styles.minHeight = minHeight
      if (maxWidth) styles.maxWidth = maxWidth
      if (width) styles.width = width
      if (minWidth) styles.minWidth = minWidth

      return styles;
    }
  }
})

export default Measurable;
