import { DirectiveBinding } from "vue/types/options";
import { VNode } from "vue";

interface ClickOutsideElement extends HTMLElement {
  __clickOutside: any
}

function validate(binding: DirectiveBinding) {
  if (typeof binding.value !== 'function') {
    console.warn('[Vue-click-outside:] provided expression', binding.expression, 'is not a function.')
    return false
  }

  return true
}

const ClickOutside = {
  bind(el: ClickOutsideElement, binding: DirectiveBinding, VNode: VNode) {
    if (!validate(binding)) return

    function handler(e: Event): void {
      if (!VNode.context) return

      // @ts-ignore
      if (VNode.context.sharedState.active && !el.contains(e.target as Node))
        binding.value();
    }

    el.__clickOutside = {
      handler: handler,
      callback: binding.value
    }

    const app = document.getElementById("__nuxt") || document.body;
    const clickHandler = 'ontouchstart' in document.documentElement ? 'touchstart' : 'click';

    app.addEventListener(clickHandler, handler)
  },

  update: function (el: ClickOutsideElement, binding: DirectiveBinding) {
    if (validate(binding)) el.__clickOutside.callback = binding.value
  },

  unbind(el: ClickOutsideElement) {
    const clickHandler = 'ontouchstart' in document.documentElement ? 'touchstart' : 'click';
    const app = document.getElementById("__nuxt") || document.body;

    el.__clickOutside && app.removeEventListener(clickHandler, el.__clickOutside.handler)
    delete el.__clickOutside
  }
}

export default ClickOutside;
