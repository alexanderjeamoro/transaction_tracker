import { Listable } from "~/core/contracts/Contractable";

export interface Currency {
  id: string,
  abbr: string,
  code: string,
  locale: string,
  name: string,
  icon: string
}

export interface CurrencyState extends Listable<Currency> {

}
