import { Listable } from "~/core/contracts/Contractable";

export interface Tag {
  id?: string,
  name: string,
}

export interface TagState extends Listable<Tag> {

}
