import { Activable, Listable } from "~/core/contracts/Contractable";

export interface Transaction {
  id?: string,
  remarks: string,
  amount: number | null,
  transactionDate: string | null,
  userId: string,
  userNickname: string,
  walletId: string,
  walletName: string,
  categoryId: string,
  categoryName: string,
  currencyId: string,
  currencyCode: string,
  tagId?: string,
  tagName?: string,
}

export interface TransactionState extends Listable<Transaction>, Activable<Transaction> {

}
