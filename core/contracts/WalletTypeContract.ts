import { Listable } from "~/core/contracts/Contractable";

export interface WalletType {
  id: string,
  name: string,
  slug: string
}

export interface WalletTypeState extends Listable<WalletType> {

}
