import { Listable } from "~/core/contracts/Contractable";

export interface Wallet {
  id?: string
  name: string,
  currencyId: string,
  currencyCode: string,
  currentBalance: number,
  transactionsCount: number,
  userId: string,
  createdAt?: string,
  updatedAt?: string,
  users?: object
}

export interface WalletState extends Listable<Wallet> {

}
