import { Listable } from "~/core/contracts/Contractable";

export interface Category {
  id?: string,
  name: string,
  isUpdatable: boolean,
  transactionCount: number,
  walletId: string,
  walletName: string,
  categoryTypeId: string,
  categoryTypeName: string,
  parentId?: string,
  parentName: string,
  children?: {
    [x: string]: string
  },
  createdAt?: string,
  updatedAt?: string
}

export interface CategoryState extends Listable<Category> {

}
