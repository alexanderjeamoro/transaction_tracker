export interface CategoryType {
  id: string,
  name: string,
}

export interface CategoryTypeState {
  list: Record<string, string>
}
