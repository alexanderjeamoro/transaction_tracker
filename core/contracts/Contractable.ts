export interface RootState {
  version?: string;

  [ x:string ]: any;
}

export interface Listable<T> {
  list: Array<T>
}

export interface PrimaryID {
  id: string
}

export interface Searchable {
  search: string | number | null;
}

export interface Selectable {
  id: bigint | string,
  text: string
}

export interface Activable<T> {
  active_item: T;
  original_active_item: T;
}
