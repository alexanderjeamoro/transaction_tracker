import { ActionTree, GetterTree } from "vuex";
import { RootState } from "~/core/contracts/Contractable";
import { Tag, TagState } from "~/core/contracts/Tag";
import { TAG_STATE_RESET } from "~/core/utils/Resetters";
import { firestoreAction } from "vuexfire";
import { db } from "~/plugins/Firebase";

export const state = () => ({
  list: [
    {id: "2j13h1cvx", name: "Mommy"},
    {id: "2j12xkzcx", name: "Dimche"}
  ]
})

export const getters: GetterTree<TagState, RootState> = {
  isListEmpty: (state): boolean => {
    return state.list.length === 0;
  },

  getList: (state): Array<Tag> => {
    return state.list;
  }
}

export const actions: ActionTree<TagState, RootState> = {
  bindTag: firestoreAction((context) => {
    const ref = db.collection("users")
      .doc(context.rootGetters["getUserId"])
      .collection('tag');

    return context.bindFirestoreRef("list", ref);
  }),


  postTag: firestoreAction((context, payload) => {
    return db.collection("users")
      .doc(context.rootGetters["getUserId"])
      .collection('tag')
      .add(payload);
  })
}
