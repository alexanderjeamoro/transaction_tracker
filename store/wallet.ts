import { ActionTree, GetterTree, MutationTree } from "vuex";
import { RootState } from "~/core/contracts/Contractable";
import { Wallet, WalletState } from "~/core/contracts/WalletContract";
import { RESET } from "~/core/utils/ExportableMutations";
import { WALLET_STATE_RESET } from "~/core/utils/Resetters";
import { firestoreAction } from "vuexfire";
import firebase from "~/plugins/Firebase";

export const state = () => WALLET_STATE_RESET()

export const getters: GetterTree<WalletState, RootState> = {
  isListEmpty: (state): boolean => {
    return state.list.length === 0;
  },

  getWallet: (state) => (id: string) => {
    return state.list.find(item => item.id === id);
  },

  getList: (state): Array<Wallet> => {
    return state.list
  }
}

export const mutations: MutationTree<WalletState> = {
  ...RESET<WalletState>(WALLET_STATE_RESET())
}

export const actions: ActionTree<WalletState, RootState> = {
  bindWallet: firestoreAction(async function ({rootGetters, bindFirestoreRef}) {
    // @ts-ignore
    const ref = this.$fire.firestore
      .collection("wallets")
      .where("userId", "==", rootGetters["getUserId"]);

    await bindFirestoreRef("list", ref, {wait: true});
  }),

  unbindWallet: firestoreAction(({unbindFirestoreRef}) => {
    unbindFirestoreRef("list")
  }),

  async getWallet({rootGetters}, walletId = "Ah75HqghSVejOzCVl2Ev") {
    const walletData = await this.$fire.firestore
      .collection("wallets")
      .doc(walletId)
      .get();

    return walletData.data();
  },

  postWallet: firestoreAction(async function (context, {currencyId, currencyCode, currentBalance, name}) {
    // @ts-ignore
    return await this.$fire.firestore
      .collection("wallets")
      .add({
        currencyId: currencyId,
        currencyCode: currencyCode,
        currentBalance: currentBalance,
        name: name,
        userId: context.rootGetters.getUserId,
        updatedAt: firebase.firestore.FieldValue.serverTimestamp(),
        createdAt: firebase.firestore.FieldValue.serverTimestamp(),
        transactionsCount: 0,
        users: {
          [context.rootGetters.getUserId]: true
        }
      });
  }),

  updateWallet: firestoreAction(async function (context, payload: Wallet) {
    // @ts-ignore
    return await this.$fire.firestore
      .collection("wallets")
      .doc(payload.id)
      .update(payload);
  }),

  addWalletTransactionCount: firestoreAction(async function (context, walletId) {
    // @ts-ignore
    return await this.$fire.firestore
      .collection("wallets")
      .doc(walletId)
      .update({
        transactionCounts: firebase.firestore.FieldValue.increment(1)
      });
  }),

  subtractWalletTransactionCount: firestoreAction(async function (context, walletId) {
    // @ts-ignore
    return await this.$fire.firestore
      .collection("wallets").doc(walletId)
      .update({
        transactionCounts: firebase.firestore.FieldValue.increment(-1)
      });
  })
}
