import { ActionTree, GetterTree, MutationTree } from "vuex";
import { RootState } from "~/core/contracts/Contractable";
import { Category, CategoryState } from "~/core/contracts/CategoryContract";
import firebase, { db } from "~/plugins/Firebase";
import { CATEGORY_STATE_RESET } from "~/core/utils/Resetters";
import { firestoreAction } from "vuexfire";

export const state = () => CATEGORY_STATE_RESET()

export const getters: GetterTree<CategoryState, RootState> = {
  isListEmpty: (state): boolean => {
    return state.list.length === 0;
  },

  getExpensesCategory: (state, getters, rootState, rootGetters): Array<Category> => {
    return state.list.filter(entry => {
      return entry.categoryTypeId === rootGetters["categoryType/getExpenseId"]
    });
  },

  getIncomeCategory: (state, getters, rootState, rootGetters): Array<Category> => {
    return state.list.filter(entry => {
      return entry.categoryTypeId === rootGetters["categoryType/getIncomeId"]
    });
  }
}

export const mutations: MutationTree<CategoryState> = {}

export const actions: ActionTree<CategoryState, RootState> = {
  bindCategory: firestoreAction((context, id) => {
    const ref = db.collection('users')
      .doc(context.rootGetters["getUserId"])
      .collection("categories");

    return context.bindFirestoreRef("list", ref);
  }),

  postCategory: firestoreAction(async (context, data: Category) => {
    const formData: Record<string, any> = {
      name: data.name,
      walletId: data.walletId,
      walletName: data.walletName,
      categoryTypeId: data.categoryTypeId,
      categoryTypeName: data.categoryTypeName,
      transactionCount: 0,
      isUpdatable: true,
      updatedAt: firebase.firestore.FieldValue.serverTimestamp(),
      createdAt: firebase.firestore.FieldValue.serverTimestamp()
    }

    if (data.parentId) {
      formData.parentId = data.parentId;
      formData.parentName = data.parentName;
    }

    return await db.collection("users")
      .doc(context.rootGetters["getUserId"])
      .collection("categories")
      .add(formData);
  }),

  addTransactionCount: firestoreAction((context, categoryId) => {
    return db.collection("users")
      .doc(context.rootGetters["getUserId"])
      .collection("categories")
      .doc(categoryId)
      .update({
        transactionsCount: firebase.firestore.FieldValue.increment(1)
      })
  }),

  SubtractTransactionCount: firestoreAction((context, categoryId) => {
    return db.collection("users")
      .doc(context.rootGetters["getUserId"])
      .collection("categories")
      .doc(categoryId)
      .update({
        transactionsCount: firebase.firestore.FieldValue.increment(-1)
      })
  })
}
