import { ActionTree, GetterTree, MutationTree } from "vuex";
import { Listable, RootState } from "~/core/contracts/Contractable";
import { CategoryType, CategoryTypeState } from "~/core/contracts/CategoryTypeContract";
import { categoryTypeCollection } from "~/plugins/Firebase";
import { CATEGORY_TYPE_STATE_RESET } from "~/core/utils/Resetters";
import { SET_LIST } from "~/core/utils/mutation-types";
import Vue from "vue";

export const state = () => CATEGORY_TYPE_STATE_RESET()

export const getters: GetterTree<CategoryTypeState, RootState> = {
  isListEmpty: (state): boolean => {
    return Object.keys(state.list).length === 0;
  },

  getList: (state): object => {
    return state.list;
  },

  getExpenseId: (state) => {
    return state.list["Expenses"];
  },

  getIncomeId: (state) => {
    return state.list["Income"];
  }
}

export const mutations: MutationTree<CategoryTypeState> = {
  [SET_LIST](state, payload: Listable<CategoryType>): void {
    Vue.set(state, "list", payload);
  },
}

export const actions: ActionTree<CategoryTypeState, RootState> = {
  async getList({commit}) {
    const querySnapshot = await categoryTypeCollection.get();
    const list: Record<string, string> = {}

    querySnapshot.forEach(doc => {
      const name = doc.data().name

      list[name] = doc.id;
    });

    commit(SET_LIST, list)
  }
}
