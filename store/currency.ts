import { ActionTree, GetterTree, MutationTree } from "vuex";
import { Listable, RootState } from "~/core/contracts/Contractable";
import { Currency, CurrencyState } from "~/core/contracts/Currency";
import { currencyCollection } from "~/plugins/Firebase";
import { CURRENCY_STATE_RESET } from "~/core/utils/Resetters";
import { SET_LIST } from "~/core/utils/mutation-types";
import Vue from "vue";

export const state = () => CURRENCY_STATE_RESET()

export const getters: GetterTree<CurrencyState, RootState> = {
  isListEmpty: (state): boolean => {
    return state.list.length === 0;
  },

  getList: (state): Array<Currency> => {
    return state.list;
  }
}

export const mutations: MutationTree<CurrencyState> = {
  [SET_LIST](state, payload: Listable<Currency>): void {
    Vue.set(state, "list", payload);
  },
}

export const actions: ActionTree<CurrencyState, RootState> = {
  getList: async ({commit}) => {
    const querySnapshot = await currencyCollection.get();
    const list: Array<Currency> = [];

    querySnapshot.forEach(doc => {
      list.push({
        id: doc.id,
        ...doc.data()
      } as Currency);
    });

    commit(SET_LIST, list)
  }
}
