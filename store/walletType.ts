import { ActionTree, GetterTree, MutationTree } from "vuex";
import { RootState } from "~/core/contracts/Contractable";
import { WalletTypeState } from "~/core/contracts/WalletTypeContract";
import { walletTypesCollection } from "~/plugins/Firebase";
import { WALLET_TYPE_STATE_RESET } from "~/core/utils/Resetters";
import { firestoreAction } from "vuexfire";

export const state = () => WALLET_TYPE_STATE_RESET()

export const getters: GetterTree<WalletTypeState, RootState> = {}

export const mutations: MutationTree<WalletTypeState> = {
}

export const actions: ActionTree<WalletTypeState, RootState> = {
  bindWalletTypes: firestoreAction(({ bindFirestoreRef}) => {
    return bindFirestoreRef(
      "list",
      walletTypesCollection.orderBy("name", "desc")
    );
  }),

  addWalletType: firestoreAction(( context, payload) => {
    return walletTypesCollection.add(payload);
  }),

  deleteWalletType: firestoreAction(( context, uid) => {
    return walletTypesCollection.doc(uid)
      .delete();
  }),

  getWalletType({commit}, {id}) {
    return walletTypesCollection
      .doc(id)
      .get();
  },

}
