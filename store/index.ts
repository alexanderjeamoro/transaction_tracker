import { ActionTree, GetterTree, MutationTree } from 'vuex'
import { auth, settingCollection } from "~/plugins/Firebase";
import { RootState } from "~/core/contracts/Contractable";
import Vue from "vue";
import { firestoreAction, vuexfireMutations } from 'vuexfire';
import { APPEND_TOASTER, DELETE_TOASTER } from "@/core/utils/mutation-types";
import { RESET } from "~/core/utils/ExportableMutations";

const initialState = () => ({
  user: {
    uid: "zLhGMAGBP2fgSIwGrKroHixwc7C2",
    email: "alexanderjeamoro@gmail.com",
    displayName: "Alex"
  },
  setting: {
    is_admin: false
  },
  title: "",

  showTransactionCreateModal: false,
  showWalletCreateModal: false,
  showCategoryCreateModal: false,
  toasters: []
});

export const state = () => initialState();

export const getters: GetterTree<RootState, RootState> = {
  isAuthenticated(state) {
    return !!state.user;
  },

  getUserId(state, getters) {
    if (getters.isAuthenticated) {
      return state.user.uid;
    }

    return null;
  },

  getDisplayName(state, getters) {
    if (getters.isAuthenticated) {
      return state.user.displayName;
    }

    return null;
  },

  showToaster(state) {
    return state.toasters.length !== 0;
  }
}

export const mutations: MutationTree<RootState> = {
  ...vuexfireMutations,
  ...RESET<RootState>(initialState()),
  SET_USER(state, payload) {
    Vue.set(state, "user", payload)
  },

  SET_USER_SETTING(state, payload) {
    Vue.set(state, "setting", payload)
  },

  SET_TITLE(state, payload) {
    state.title = payload;
  },

  TOGGLE_GLOBAL_MODAL(state, key) {
    state[key] = !state[key]
  },

  [APPEND_TOASTER](state, {message, status = 'success'}) {
    state.toasters.push({
      id: (new Date()).getTime(),
      message: message,
      status: status,
    });
  },

  [DELETE_TOASTER](state, id) {
    state.toasters = state.toasters.filter((toaster: any) => toaster.id !== id);
  }
}

export const actions: ActionTree<RootState, RootState> = {
  async signUpWithEmail({commit, dispatch}, {credentials, setting}) {
    try {
      await auth.createUserWithEmailAndPassword(credentials.email, credentials.password);

      const currentUser = auth.currentUser;

      await currentUser?.sendEmailVerification()
        .then(() => {
          console.log("Email Sent");
        });

      dispatch("storeUserSetting", {
        uid: currentUser?.uid,
        is_admin: setting.is_admin
      });
    } catch (error) {
      if (error.code === 'auth/email-already-in-use') {
        dispatch("signInWithEmailAndPassword", credentials);
      }
    }
  },

  async signInWithEmailAndPassword({state, commit, dispatch}, {email, password}) {
    try {
      await auth.signInWithEmailAndPassword(email, password);
      const currentUser = auth.currentUser;

      if (!currentUser?.emailVerified) {
        throw new Error('Account is not verified');
      }

      const {uid} = currentUser;

      commit("SET_USER", {
        uid, email
      });

      dispatch("getUserSetting", uid);

      return currentUser;
    } catch (error) {
      if (error.code === 'auth/user-not-found' || error.code === 'auth/wrong-password')
        throw new Error('These credentials do not match our records.');
      if (error.code === 'auth/too-many-requests')
        throw new Error('Access to this account has been temporarily disabled due to many failed login attempts.');
    }
  },

  storeUserSetting: firestoreAction((context, credentials) => {
    return settingCollection.doc(credentials.uid)
      .set({
        is_admin: credentials.is_admin
      });
  }),

  getUserSetting: firestoreAction(({commit, bindFirestoreRef}, uid) => {
    return bindFirestoreRef(
      'setting',
      settingCollection.doc(uid)
    );
  }),

  signOut() {
    return auth.signOut()
      .then(response => {

      });
  },

  showTransactionCreateModal({commit}, payload) {
    commit("transaction/SET_ACTIVE_ITEM_VALUE_BY_KEY", payload);
    commit("TOGGLE_GLOBAL_MODAL", 'showTransactionCreateModal');
  }
}
