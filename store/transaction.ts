import { ActionTree, GetterTree, MutationTree } from "vuex";
import { Transaction, TransactionState } from "~/core/contracts/Transactions";
import { RootState } from "~/core/contracts/Contractable";
import { db, transactionsCollection, walletsCollection } from "~/plugins/Firebase";
import { TRANSACTION_RESET_STATE } from "~/core/utils/Resetters";
import { ACTIVE_ITEM, RESET } from "~/core/utils/ExportableMutations";
import { firestoreAction } from "vuexfire";
import firebase from "firebase/app";

export const state = () => TRANSACTION_RESET_STATE();

export const getters: GetterTree<TransactionState, RootState> = {
  getTransactionWallets: (state) => {
    const wallets = state.list
      .map(transaction => transaction.walletId)
      .filter(transaction => transaction !== undefined);

    return [...new Set(wallets)];
  },

  getList: (state) => {
    return state.list;
  },

  getTransactionCategories: (state) => {
    const categories = state.list
      .map(transaction => transaction.categoryId)
      .filter(transaction => transaction !== undefined);

    return [...new Set(categories)];
  },
}

export const mutations: MutationTree<TransactionState> = {
  ...RESET<TransactionState>(TRANSACTION_RESET_STATE()),
  ...ACTIVE_ITEM<TransactionState, Transaction>()
}

export const actions: ActionTree<TransactionState, RootState> = {
  bindTransactions: firestoreAction(async function (context) {
    // this.$fire.firestore.collectionGroup("transactions")
    const ref = db.collectionGroup("transactions")
      .where("userId", "==", context.rootGetters["getUserId"]);

    await context.bindFirestoreRef("list", ref, {wait: true});
  }),

  unbindTransaction: firestoreAction(({unbindFirestoreRef}) => {
    unbindFirestoreRef("list");
  }),

  getWalletTransaction: firestoreAction(async function ({bindFirestoreRef}, walletId) {
    // @ts-ignore
    const ref = this.$fire.firestore
      .collection("wallets")
      .doc(walletId)
      .collection("transactions");

    await bindFirestoreRef("list", ref, {wait: true});
  }),

  addTransaction: firestoreAction(async function ({rootGetters, dispatch}, transaction) {
    transaction.userId = rootGetters["getUserId"];
    transaction.userNickname = rootGetters["getDisplayName"];
    transaction.createdAt = firebase.firestore.FieldValue.serverTimestamp();
    transaction.updatedAt = firebase.firestore.FieldValue.serverTimestamp();

    const addedTransaction = await walletsCollection
      .doc(transaction.walletId)
      .collection("transactions")
      .add(transaction);

    await dispatch("category/addTransactionCount", transaction.categoryId, {root: true});
    await dispatch("wallet/addWalletTransactionCount", transaction.walletId, {root: true});

    return addedTransaction;
  }),

  updateTransaction: firestoreAction((_, payload) => {
    return walletsCollection
      .doc(payload.walletId)
      .collection("transactions")
      .doc(payload.id)
      .update(payload)
  }),

  deleteTransaction: firestoreAction(async function ({dispatch}, transaction) {
    const deletedTransaction = await transactionsCollection
      .doc(transaction.id)
      .delete()

    await dispatch("wallet/subtractWalletTransactionCount", transaction.walletId, {root: true});
    await dispatch("category/subtractTransactionCount", transaction.categoryId, {root: true});

    return deletedTransaction;
  }),

  async getTransaction(_, {walletId, transactionId}) {
    return await this.$fire.firestore
      .collection('wallets')
      .doc(walletId)
      .collection('transactions')
      .doc(transactionId)
      .get();
  }
}
