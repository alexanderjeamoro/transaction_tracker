export default [{
  "id": "ABqKaGo9FBj02gfj44TD",
  "code": "USD",
  "icon": "/countries/united-states.png",
  "name": "United States Dollar",
  "locale": "en-US",
  "abbr": "$"
}, {
  "id": "ZmwcEI0ZAmlJYUlLFfnK",
  "abbr": "₱",
  "locale": "en-US",
  "code": "PHP",
  "icon": "/countries/philippines.png",
  "name": "Philippine Pesos"
}, {
  "id": "h9mS94o8r4ArCqd3RRr5",
  "code": "EUR",
  "name": "Euro",
  "locale": "en-US",
  "icon": "/countries/european-union.png",
  "abbr": "€"
}];
