export default [{
  "currencyCode": "USD",
  "currentBalance": "555",
  "transactionsCount": 0,
  "name": "Alex",
  "userId": "zLhGMAGBP2fgSIwGrKroHixwc7C2",
  "currencyId": "ABqKaGo9FBj02gfj44TD",
  "users": {"zLhGMAGBP2fgSIwGrKroHixwc7C2": 1},
  "updatedAt": {"seconds": 1617075393, "nanoseconds": 276000000}
}, {
  "updatedAt": {"seconds": 1617699949, "nanoseconds": 579000000},
  "userId": "zLhGMAGBP2fgSIwGrKroHixwc7C2",
  "transactionsCount": 0,
  "currencyId": "ABqKaGo9FBj02gfj44TD",
  "currencyCode": "USD",
  "users": {"zLhGMAGBP2fgSIwGrKroHixwc7C2": true},
  "createdAt": {"seconds": 1617699949, "nanoseconds": 579000000},
  "name": "Sampling",
  "currentBalance": "55"
}, {
  "currencyCode": "PHP",
  "currencyId": "ZmwcEI0ZAmlJYUlLFfnK",
  "name": "Cardano",
  "users": {"zLhGMAGBP2fgSIwGrKroHixwc7C2": 1},
  "updatedAt": {"seconds": 1616777223, "nanoseconds": 59000000},
  "userId": "zLhGMAGBP2fgSIwGrKroHixwc7C2",
  "transactionsCount": 0,
  "currentBalance": 1000000
}, {
  "walletType": {"uid": "98gft2XfhajdcnPLeZf4", "name": "Joint"},
  "currency": "PHP",
  "currentBalance": 5000,
  "currencyCode": "PHP",
  "updatedAt": {"seconds": 1614614400, "nanoseconds": 0},
  "transactionCount": 0,
  "userId": "zLhGMAGBP2fgSIwGrKroHixwc7C2",
  "users": {
    "zLhGMAGBP2fgSIwGrKroHixwc7C2": 1,
    "e643iLlJ1vgoCSuHMnHADpRQvVF3": {"name": "Alex", "type": 1, "email": "alexanderjeamoro@gmail.com"}
  },
  "name": "BPI",
  "createdAt": {"seconds": 1614528000, "nanoseconds": 0},
  "currencyId": "ZmwcEI0ZAmlJYUlLFfnK"
}, {
  "transactionsCount": 0,
  "currentBalance": 99,
  "name": "sada",
  "userId": "zLhGMAGBP2fgSIwGrKroHixwc7C2",
  "updatedAt": {"seconds": 1616776520, "nanoseconds": 841000000},
  "currencyCode": "PHP",
  "users": {"zLhGMAGBP2fgSIwGrKroHixwc7C2": 1},
  "currencyId": "ZmwcEI0ZAmlJYUlLFfnK"
}]
