export default {
  // Target: https://go.nuxtjs.dev/config-target
  target: 'static',

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    titleTemplate: '%s - frontend',
    title: 'frontend',
    meta: [
      {charset: 'utf-8'},
      {name: 'viewport', content: 'width=device-width, initial-scale=1'},
      {hid: 'description', name: 'description', content: ''}
    ],
    link: [
      {rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'}
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '@/assets/main.scss'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    {src: '@/plugins/Vuelidate'},
    {src: '@/plugins/Firebase'},
    {src: '@/plugins/Helpers'},
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: [
    {path: '~/components/VForm', extensions: ['vue']},
    {path: '~/components/VButton', extensions: ['vue']},
    {path: '~/components/VContainer', extensions: ['vue']},
    {path: '~/components/VTab', extensions: ['vue']},
    {path: '~/components/VDropdown', extensions: ['vue']},
    {path: '~/components', extensions: ['vue']}
  ],

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    "@nuxt/components",
    '@nuxt/typescript-build',
    '@nuxt/image',
    '@nuxtjs/tailwindcss',
    '@nuxtjs/fontawesome',
    '@nuxtjs/dotenv',
  ],

  dotenv: {
    filename: '.env.prod'
  },

  fontawesome: {
    component: 'fa',
    suffix: true,
    icons: {
      solid: [
        'faUser',
        'faTimes',
        'faAngleDown',
        'faBell',
        'faSearch',
        'faPlusSquare',
        'faTachometerAlt',
        'faShoppingBag',
        'faQuestionCircle',
        'faPen',
        'faMinusCircle',
        'faTrash',
        'faAngleLeft',
        'faAngleRight',
        'faUndo',
        'faEllipsisV',
        'faEllipsisH',
        'faPlus',
        'faRandom',
        'faClipboard',
        'faMobile',
        'faMoneyBill',
        'faSpinner',
        'faBars',
        'faWallet',
        'faEye',
        'faChevronLeft',
        'faChevronRight',
      ],
      regular: [
        'faEdit',
        'faCopy'
      ]
    }
  },

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
    '@nuxtjs/dayjs',
    '@nuxtjs/firebase'
  ],

  firebase: {
    config: {
      apiKey: process.env.API_KEY,
      authDomain: process.env.AUTH_DOMAIN,
      projectId: process.env.PROJECT_ID,
      storageBucket: process.env.STORAGE_BUCKET,
      messagingSenderId: process.env.MESSAGING_SENDER_ID,
      appId: process.env.APP_ID,
      measurementId: process.env.MEASUREMENT_ID
    },
    services: {
      auth: true,
      firestore: true,
    }
  },

  dayjs: {
    locales: ['en'],
    defaultLocale: 'en',
    defaultTimeZone: 'Asia/Manila',
    plugins: [
      'utc',
      'timezone',
      'isToday'
    ]
  },

  // PWA module configuration: https://go.nuxtjs.dev/pwa
  pwa: {
    manifest: {
      lang: 'en'
    }
  },

  tailwindcss: {
    configPath: '~/tailwind.config.js',
    jit: true
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {}
}
