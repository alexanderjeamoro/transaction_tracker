import mixins from "~/core/utils/mixins";
import Proxyable from "~/core/mixins/proxyable";
import { find } from "lodash"

const BaseMixin = mixins(
  Proxyable
)

type optionType = {
  [x: string]: any
}

const VSelectMixin = BaseMixin.extend({
  name: "VSelectMixin",

  props: {
    placeholder: {
      type: String,
      required: false,
      default: "Select"
    },

    clearable: Boolean
  },

  data: () => ({
    isApiLoading: false,
    search: "",
    dataValue: "id",
    dataText: "name"
  }),

  computed: {
    getText() {
      if (this.getSelected) {
        const option: optionType = this.getSelected;

        return option[this.dataText as string];
      }

      return this.placeholder
    },

    getSelected(): optionType | undefined {
      if (!this.internalValue) return undefined;

      return find(this.options, [this.dataValue as string, this.internalValue]);
    }
  },

  methods: {
    resetSearch(): void {
      this.search = "";
    },

    selectedOption(selected: any, close: any) {
      // @ts-ignore
      this.resetSearch();
      this.$emit('change', selected);
      close()
    }
  }
});

export default VSelectMixin;
