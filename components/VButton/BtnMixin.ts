import mixins from "~/core/utils/mixins";
import Colorable from "~/core/mixins/colorable";
import Sizeable from "~/core/mixins/sizeable";
import Measurable from "~/core/mixins/measurable";

const types = ['button', 'reset', 'submit'];

const BaseMixins = mixins(
  Colorable,
  Sizeable,
  Measurable,
);

const BtnMixin = BaseMixins.extend({
  props: {
    type: {
      type: String,
      default: "button",
      validator(value): boolean {
        return types.includes(value);
      }
    },

    active: Boolean,
    outlined: Boolean,
    text: Boolean,
    disabled: Boolean,
    fullWidth: Boolean,

    rounded: Boolean,
    elevation: Boolean,
  },

  computed: {
    BtnMixinClasses(): Record<string, boolean> {
      return {
        "v-btn": true,
        "w-full": this.fullWidth,
        outlined: this.outlined,
        text: this.text,
        rounded: this.rounded,
        shadow: this.elevation,
        ...this.colorClasses,
        ...this.sizeableClasses
      }
    },

    BtnMixinStyles(): object {
      return {
        ...this.measurableStyles
      }
    }
  }
})

export default BtnMixin;

