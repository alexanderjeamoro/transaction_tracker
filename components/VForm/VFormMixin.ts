import Roundable from "~/core/mixins/roundable";
import { factory as ProxyableFactory } from "~/core/mixins/proxyable";
import mixins from "~/core/utils/mixins";

const baseMixins = mixins(
  Roundable,
  ProxyableFactory("value", "input")
)

const VForm = baseMixins.extend({
  props: {
    errorMessages: {
      type: [String, Array],
      default: null
    },

    value: [String, Number],

    hint: String,
    placeholder: String,
    autocomplete: String,
    label: String,
    id: String,

    disabled: Boolean,
    fullWidth: Boolean,
    required: Boolean,
    loading: Boolean,
    hideDetails: Boolean,
  },

  computed: {
    hasMessages(): boolean {
      return !this.hideDetails && Boolean(
        this.hint || this.hasError
      );
    },

    hasError(): boolean {
      return Boolean(this.getErrorMessages.length);
    },

    getMessages(): Array<string> {
      if (this.hasError) {
        return this.getErrorMessages
      }

      return [this.hint];
    },

    getErrorMessages(): Array<string> {
      let errors: Array<string> = [];

      if (Array.isArray(this.errorMessages) && this.errorMessages.length) {
        errors = this.errorMessages.filter(Boolean) as Array<string>;
      } else if (typeof this.errorMessages === 'string') {
        errors.push(this.errorMessages);
      }

      return errors;
    },

    formGroupClasses(): Record<string, boolean> {
      return {
        'v-form-group': true,
        'danger': this.hasError
      }
    }
  },

  methods: {
    getUniqueKey(index: number) {
      return (new Date()).getTime() + index;
    }
  }
});

export default VForm;
